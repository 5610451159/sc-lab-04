package model;

public class Asterisk {
	private String answer = "";
	
	public String format1(int number) {
		for (int i = 1 ; i <= number ; i++) {
			for (int j = 1 ; j <= number ; j++) {
				answer += "*";
			}
			answer += "\n";
		}
		return answer;
	}
	
	public String format2(int number) {
		for (int i = 1 ; i <= number ; i++) {
			for (int j = 1 ; j <= i ; j++) {
				answer += "*";
			}
			answer += "\n";
		}
		return answer;
	}
	
	public String format3(int number) {
		for (int i = 1 ; i <= number ; i++) {
			for (int j= 1 ; j <= number ; j++) {
				if (j%2 == 0) {
					answer += "*";
				}
				else {
					answer += "-";
				}
			}
			answer += "\n";
		}
		return answer;
	}
	
	public String format4(int number) {
		for (int i = 1 ; i <= number ; i++) {
			for (int j= 1 ; j <= number ; j++) {
				if ((i+j)%2 == 0) {
					answer += "*";
				}
				else {
					answer += " ";
				}
			}
			answer += "\n";
		}
		return answer;
	}
}
