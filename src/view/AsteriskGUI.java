package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class AsteriskGUI extends JFrame {
	private JPanel panel1;
	private JPanel panel2;
	private JButton button;
	private JComboBox combobox;
	private String format[] = {"format1","format2","format3","format4"};
	private JLabel label;
	private JTextField textfield;
	private JTextArea textarea;
	
	public void createPanel1() {
		//panel1 �����ǹ�ͧ��� Input		
	    panel1 = new JPanel();
	    add(panel1);
	    panel1.add(label);
	    panel1.add(textfield);
	    panel1.add(combobox);
	    panel1.add(button, BorderLayout.SOUTH);
	    
	    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    setResizable(false);
		setVisible(true);
		setBounds(550, 200, 300, 400);
	} 
	
	public void createPanel2() {
		//panel1 �����ǹ�ͧ��� Output
	    panel2 = new JPanel();
	    panel2.setLayout(new BorderLayout());
	    add(panel2);
	    panel2.add(textarea, BorderLayout.CENTER);
	}
	
	public void createButton(ActionListener listener) {
	    button = new JButton("Enter");
	    button.addActionListener(listener);
	}
	
	public void createCombobox() {
		combobox = new JComboBox(format);
		combobox.setPreferredSize(new Dimension(110, 20));
	}
	
	public String getCombobox() {
		String format = (String) combobox.getSelectedItem();
		return format;
	}
	
	public void createLabel() {
		label = new JLabel("-- ��س�������Ţ ��� ���͡�ٻẺ --");
	}
	
	public void createTextField() {
		textfield = new JTextField(10);
	}
	
	public int getTextField() {
		String text = textfield.getText();
		int number = Integer.parseInt(text);
		return number;
	}

	public void createTextArea() {
		textarea = new JTextArea();
	}
	
	public void setTextArea(String str) {
		textarea.setText(str);
	}
}