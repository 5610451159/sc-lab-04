package control;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import model.Asterisk;
import view.AsteriskGUI;

public class AsteriskControl {
	private AsteriskGUI GUI = new AsteriskGUI();
	private Asterisk model = new Asterisk();
	
	class AddInterestListener implements ActionListener {
    	public void actionPerformed(ActionEvent event) {
    		if (GUI.getCombobox() == "format1") {
    			GUI.setTextArea(model.format1(GUI.getTextField()));	
    		}
    		
    		else if (GUI.getCombobox() == "format2") {
    			GUI.setTextArea(model.format2(GUI.getTextField()));
    		}
    		
    		else if (GUI.getCombobox() == "format3") {
    			GUI.setTextArea(model.format3(GUI.getTextField()));
    		}
    		
    		else if (GUI.getCombobox() == "format4") {
    			GUI.setTextArea(model.format4(GUI.getTextField()));
    		}
        }            
    }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new AsteriskControl();
	}	
	
	public AsteriskControl() {
		listener = new AddInterestListener();
		GUI.createButton(listener);
		GUI.createCombobox();
		GUI.createLabel();
		GUI.createTextField();
		GUI.createTextArea();	
		GUI.setLayout(new GridLayout(2, 1));
		GUI.createPanel1();
		GUI.createPanel2();
	}

	ActionListener listener;
}
